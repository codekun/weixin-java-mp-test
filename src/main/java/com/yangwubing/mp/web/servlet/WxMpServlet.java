package com.yangwubing.mp.web.servlet;

import java.io.FileInputStream;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import me.chanjar.weixin.common.util.StringUtils;
import me.chanjar.weixin.common.util.crypto.SHA1;
import me.chanjar.weixin.mp.api.WxMpInMemoryConfigStorage;
import me.chanjar.weixin.mp.api.WxMpMessageRouter;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.api.WxMpServiceImpl;
import me.chanjar.weixin.mp.bean.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.WxMpXmlOutMessage;
import me.chanjar.weixin.mp.util.crypto.WxMpCryptUtil;

import org.apache.commons.io.IOUtils;

import com.yangwubing.mp.message.handers.WxMpMessageHandlerRuleInilizer;
import com.yangwubing.mp.message.route.NotChectedDublicatedWxMpMessageRouter;

public class WxMpServlet extends HttpServlet {
	
	protected WxMpInMemoryConfigStorage wxMpConfigStorage;
	
	protected WxMpService wxMpService;
	
	protected WxMpMessageRouter wxMpMessageRouter;

	@Override
	public void init() throws ServletException {
		super.init();

		wxMpConfigStorage = new WxMpInMemoryConfigStorage();
		wxMpConfigStorage.setAppId("wx03eb21d7ae509e94"); //
		wxMpConfigStorage.setSecret("4a88f43c63dae8196788096dd5a686b8"); 
		wxMpConfigStorage.setToken("asdf12345"); 
		wxMpConfigStorage.setAesKey("Lef10OLwQggXDSirL9gL1zK2iH1dXhCNF1hD9NsGkjf"); 
		wxMpService = new WxMpServiceImpl();
		
		wxMpService.setWxMpConfigStorage(wxMpConfigStorage);
		
		wxMpMessageRouter = new NotChectedDublicatedWxMpMessageRouter(wxMpService);
		WxMpMessageHandlerRuleInilizer.init(wxMpMessageRouter);
	}

	@Override
	protected void service(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		response.setContentType("text/html;charset=utf-8");
		response.setStatus(HttpServletResponse.SC_OK);

		String signature = request.getParameter("signature");
		String nonce = request.getParameter("nonce");
		String timestamp = request.getParameter("timestamp");

		if (!wxMpService.checkSignature(timestamp, nonce, signature)) {
			
			response.getWriter().println("非法请求");
			return;
		}

		String echostr = request.getParameter("echostr");
		if (StringUtils.isNotBlank(echostr)) {
			response.getWriter().println(echostr);
			return;
		}

		String encryptType = StringUtils.isBlank(request
				.getParameter("encrypt_type")) ? "raw" : request
				.getParameter("encrypt_type");

		if ("raw".equals(encryptType)) {
			WxMpXmlMessage inMessage = WxMpXmlMessage.fromXml(request
					.getInputStream());
			WxMpXmlOutMessage outMessage = wxMpMessageRouter.route(inMessage);
			response.getWriter().write(outMessage.toXml());
			return;
		}

		if ("aes".equals(encryptType)) {
			String msgSignature = request.getParameter("msg_signature");
			WxMpXmlMessage inMessage = WxMpXmlMessage.fromEncryptedXml(
					request.getInputStream(), wxMpConfigStorage, timestamp,
					nonce, msgSignature);
			WxMpXmlOutMessage outMessage = wxMpMessageRouter.route(inMessage);
			response.getWriter().write(
					outMessage.toEncryptedXml(wxMpConfigStorage));
			return;
		}

		response.getWriter().println("非法请求");
		return;
	}
	
	public static void main(String[] args) throws Exception {
		WxMpServlet servlet = new WxMpServlet();
		servlet.init();
		
		String timestamp = "1429441884";
		String nonce = "6CScsLACIIrDaNCj";
		String msgSignature = "8a108f7c79acbe6303c90698bc535f71fc350b0e";
		
		WxMpCryptUtil pc = new WxMpCryptUtil(servlet.wxMpConfigStorage);
		
		System.out.println(SHA1.gen(servlet.wxMpConfigStorage.getToken(), timestamp, nonce));
		
		//String message = IOUtils.toString(new FileInputStream("C:\\message.xml"));
		//String enctyedMessage = IOUtils.toString(new FileInputStream("C:\\enctyedMessage.xml"));
		
		
		//WxMpXmlMessage wxMpXmlMessage = WxMpXmlMessage.fromEncryptedXml(enctyedMessage, servlet.wxMpConfigStorage, timestamp, nonce, msgSignature);
		//System.out.println(wxMpXmlMessage.toString());
		
		//System.out.println(pc.encrypt(message));
	}
}
