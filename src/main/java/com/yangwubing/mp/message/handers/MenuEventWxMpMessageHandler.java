package com.yangwubing.mp.message.handers;

import java.util.Map;

import me.chanjar.weixin.common.api.WxConsts;
import me.chanjar.weixin.common.exception.WxErrorException;
import me.chanjar.weixin.common.session.WxSessionManager;
import me.chanjar.weixin.mp.api.WxMpMessageHandler;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.WxMpXmlOutMessage;

/**
 * 菜单事件类消息处理器。
 * @author Administrator
 *
 */
public class MenuEventWxMpMessageHandler implements WxMpMessageHandler {

	public WxMpXmlOutMessage handle(WxMpXmlMessage wxMessage,
			Map<String, Object> context, WxMpService wxMpService,
			WxSessionManager sessionManager) throws WxErrorException {
		if(wxMessage!=null && wxMessage.getMsgType()!=null && wxMessage.getMsgType().equals(WxConsts.XML_MSG_EVENT)){
			String eventKey = wxMessage.getEventKey();
			
			return WxMpXmlOutMessage.TEXT().content("你刚才点击了key:"+eventKey).fromUser(wxMessage.getToUserName()).toUser(wxMessage.getFromUserName()).build();
		}
		return null;
	}
}
