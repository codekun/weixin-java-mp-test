package com.yangwubing.mp.message.handers;

import me.chanjar.weixin.common.api.WxConsts;
import me.chanjar.weixin.mp.api.WxMpMessageRouter;

/**
 * 初始化消息处理器规则。
 * @author Administrator
 *
 */
public class WxMpMessageHandlerRuleInilizer {

	public static void init(WxMpMessageRouter router){
		router.rule().async(false).msgType(WxConsts.XML_MSG_TEXT).handler(new TextWxMpMessageHandler()).end();
		
		router.rule().async(false).msgType(WxConsts.XML_MSG_EVENT).handler(new MenuEventWxMpMessageHandler()).end();
	}
	
}
