package com.yangwubing.mp.message.handers;

import java.util.Map;

import me.chanjar.weixin.common.api.WxConsts;
import me.chanjar.weixin.common.exception.WxErrorException;
import me.chanjar.weixin.common.session.WxSessionManager;
import me.chanjar.weixin.mp.api.WxMpMessageHandler;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.WxMpXmlOutMessage;

/**
 * 文本消息处理器。
 * @author Administrator
 *
 */
public class TextWxMpMessageHandler implements WxMpMessageHandler {

	public WxMpXmlOutMessage handle(WxMpXmlMessage wxMessage,
			Map<String, Object> context, WxMpService wxMpService,
			WxSessionManager sessionManager) throws WxErrorException {
		if(wxMessage!=null && wxMessage.getMsgType()!=null && wxMessage.getMsgType().equals(WxConsts.XML_MSG_TEXT)){
			return WxMpXmlOutMessage.TEXT().content("发送的消息内容是："+wxMessage.getContent()+"\nxxx,你好，你查询的帐号信息余额是：1000.30，你已经使用了多少3000，还剩下3333.").fromUser(wxMessage.getToUserName()).toUser(wxMessage.getFromUserName()).build();
		}
		return null;
	}

}
