package com.yangwubing.mp.message.route;

import me.chanjar.weixin.mp.api.WxMpMessageRouter;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.WxMpXmlMessage;

/**
 * 不去重的微信消息路由器。当messageId重复的时候已经重新处理该消息，
 * 业务逻辑处理要支持幂等性，支持重复消费消息的特性。
 * @author Administrator
 *
 */
public class NotChectedDublicatedWxMpMessageRouter extends WxMpMessageRouter {

	public NotChectedDublicatedWxMpMessageRouter(WxMpService wxMpService) {
		super(wxMpService);
	}

	@Override
	protected boolean isDuplicateMessage(WxMpXmlMessage wxMessage) {
		// always return true.
		return false;
	}
	
	

}
